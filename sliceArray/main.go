package slicearray

func Sum(n []int) int {
	sum := 0
	for _, val := range n {
		sum += val
	}
	return sum
}

func SumAll(numbersToSum ...[]int) []int {
	var sums []int
	for _, numbers := range numbersToSum {
		sums = append(sums, Sum(numbers))
	}
	return sums
}

func SumTails(arrays ...[]int) []int {
	var sums []int

	for _, numbersToSum := range arrays {
		if len(numbersToSum) == 0 {
			sums = append(sums, 0)
		} else {
			tail := numbersToSum[1:]
			sums = append(sums, Sum(tail))
		}

	}
	return sums
}

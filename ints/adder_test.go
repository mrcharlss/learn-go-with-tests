package ints

import (
	"fmt"
	"testing"
)

func TestAdder(t *testing.T) {
	sum := Add(2, 2)
	expected := 4

	if sum != expected {
		t.Errorf("expected -> '%d' got -> '%d'", expected, sum)
	}
}

// Examples are like tests and they appear in the docs, you need the Output line for them to work
func ExampleAdd() {
	sum := Add(2, 2)
	fmt.Println(sum)
	//Output: 4
}

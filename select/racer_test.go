package racer

import (
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
)

func TestRacer(t *testing.T) {

	t.Run("returns an Error if server doesnt responds within specified time", func(t *testing.T) {
		serverA := makeDelayedServer(20 * time.Millisecond)

		defer serverA.Close()

		_, err := ConfigurableRacer(serverA.URL, serverA.URL, 20*time.Millisecond)
		if err == nil {
			t.Error("Expected error but didnt get one")
		}
	})

	t.Run("compares speeds of servers, returning the url of the fastest one", func(t *testing.T) {
		serverA := makeDelayedServer(20 * time.Millisecond)
		serverB := makeDelayedServer(0 * time.Millisecond)

		defer serverA.Close()
		defer serverB.Close()

		slowUrl := serverA.URL
		fastUrl := serverB.URL

		want := fastUrl
		got, err := Racer(slowUrl, fastUrl)
		if err != nil {
			t.Fatalf("did not expected error but got one: %v", err)
		}
		if got != want {
			t.Errorf("got %q, want %q", got, want)
		}
	})

}

func makeDelayedServer(delay time.Duration) *httptest.Server {
	return httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		time.Sleep(delay)
		rw.WriteHeader(http.StatusOK)
	}))
}

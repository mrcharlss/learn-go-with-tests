package iteration

var repeateTimes = 5

func Repeat(character string, repeats int) string {
	var repeated string
	if repeateTimes != repeats {
		repeateTimes = repeats
	}
	for i := 0; i < repeateTimes; i++ {
		repeated += character
	}
	return repeated
}

package iteration

import (
	"fmt"
	"testing"
)

func TestRepeat(t *testing.T) {
	repeats := 5
	repeated := Repeat("a", repeats)
	expected := repeats
	if len(repeated) != expected {
		t.Errorf("expected -> %q but got -> %q", expected, repeated)
	}
}

func ExampleRepeat() {
	repeats := 5
	repeated := Repeat("a", repeats)
	fmt.Printf("%q", repeated)
	//Output: "aaaaa"
}

func BenchmarkRepeat(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Repeat("a", 5)
	}
}

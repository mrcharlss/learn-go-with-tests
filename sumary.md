## Resumen de la actividad

Al resolver distintos problemas con un nuevo lenguaje de programación me ví forzado a pensar fuera de la caja.
Debido a que al tener un set de herramientas o skills limitados (al ser un lenguaje nuevo para mí), esto me ofreció la oportunidad de analizar el problema en sí desde otro punto de vista y a la vez tuve que analizar el paradigma que el nuevo lenguaje ofrece y llegar a una solución por un camino diferente al que hubiese llegado con el lenguaje que ya conozco.

- Entender a profundidad el problema a resolver.
- Entender como funciona el nuevo lenguaje, que herramientas tengo a la mano para resolver el problema.
- Forzarme a pensar de manera distinta a la normal, al no utilizar herramientas conocidas, por el método que ya conozco.
- Al tener un enfoque distinto puedo ofrecer una nueva perspectiva para resolver problemas dentro del proyecto.
- Un entendimiento de nivel más profundo sobre el funcionamiento de un programa o aplicación gracias a que Go toca temas "low level".
